import unittest
from app.calculator import Calculator

class TestCalculatorAdd(unittest.TestCase):
        def test_add_positive(self):
            result = Calculator.add(1,100)
            self.assertEqual(result, 101)
        def test_add_negative(self):
            result = Calculator.add(-1,-100)
            self.assertEqual(result, -101)
        def test_add_string(self):
            result = Calculator.add(1,"a")
            self.assertEqual(result, -101)
            
class TestCalculatorDivide(unittest.TestCase):
        def test_divide_positive(self):
            result = Calculator.divide(4,2)
            self.assertEqual(result, 2)
        def test_divide_byzero(self):
            result = Calculator.divide(4,0)
            self.assertIsNone(result)

if __name__ == '__main__':
    unittest.main()
